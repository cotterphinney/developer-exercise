class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    words = str.split(' ')
    words.map! do |w| 
      # separate word and punctuation
      word_and_punctuation = w.split(/(\.|\?|\!|\;|\:)/)
      word = word_and_punctuation.first
      # don't do anything if word is 4 characters or less
      next w if word.length <= 4
      # else check whether first character of word is capital and replace appropriately
      word =~ /^[A-Z]/ ? word_and_punctuation[0] = 'Marklar' : word_and_punctuation[0] = 'marklar'
      word_and_punctuation.join('')
    end
    words.join(' ')
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    sum_evens = 0
    new, old = 1, 0
    (nth - 1).times do |n|
      new, old = new + old, new
      sum_evens += new if new.even?
    end
    sum_evens
  end

end